﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyDegree.Models;

namespace MyDegree.Data
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser> {
    public ApplicationDbContext (DbContextOptions<ApplicationDbContext> options) : base (options) {

            // Eager loading - related data is loaded from DB as part of initial query.
            // Explicit loading- related data is explicitly loaded from DB later.
            // Lazy loading - related data is transparently loaded from DB when navigation property is accessed 

            // Lazy loading and serialization don’t mix well - if not careful you can end up querying your entire database 
            // just because lazy loading is enabled.

            // Lazy loading of a nav property can be turned ON by making the property virtual
            // Lazy loading of a nav property can be turned OFF by making the property non-virtual

        }

        // public properties available from this context object
        // create a DbSet for each model

        // Model type is singular
        // DbSet name is plural

        public DbSet<DegreeStatus> DegreeStatuses { get; set; }
    public DbSet<RequirementStatus> RequirementStatuses { get; set; }
    public DbSet<Degree> Degrees { get; set; }
    public DbSet<Student> Students { get; set; }
    public DbSet<DegreeRequirement> DegreeRequirements { get; set; }
    public DbSet<StudentDegreePlan> StudentDegreePlans { get; set; }
    public DbSet<PlanTerm> PlanTerms { get; set; }
    public DbSet<PlanTermRequirement> PlanTermRequirements { get; set; }

    protected override void OnModelCreating (ModelBuilder builder) {
      base.OnModelCreating (builder);

      // by default, table names will match the DbSet names and be plural
      // override the default behavior by setting the table names explicitly to be singular
      builder.Entity<DegreeStatus> ().ToTable ("DegreeStatus");
      builder.Entity<RequirementStatus> ().ToTable ("RequirementStatus");
      builder.Entity<Degree> ().ToTable ("Degree");
      builder.Entity<Student> ().ToTable ("Student");

      builder.Entity<DegreeRequirement> ().ToTable ("DegreeRequirement");
      builder.Entity<StudentDegreePlan> ().ToTable ("StudentDegreePlan");
      builder.Entity<PlanTerm> ().ToTable ("PlanTerm");
      builder.Entity<PlanTermRequirement> ().ToTable ("PlanTermRequirement");
    }
  }
}